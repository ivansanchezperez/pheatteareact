import React  from 'react';
import Header from './Header';
import Footer from './Footer';

const Layout = ({ children, ...rest }) => {
    return (
      <div>
        <Header />
        <div className="container is-fluid">
          {children}
        </div>
        <Footer />
      </div>
    )
  }

export default Layout;