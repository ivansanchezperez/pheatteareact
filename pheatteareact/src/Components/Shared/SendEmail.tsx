import React from 'react';
import emailjs from 'emailjs-com';
import {  faCheck, faTimes, faShare, faAt, faPaperPlane } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import _ from 'lodash';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class SendEmail extends React.Component<any,any>{
    constructor(props: any) {
        super(props);
        this.state = { feedback: '', from_name: '', email: '', isEmailCorrect: null, subject: '' };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      };

	handleChange(event) {
	this.setState({feedback: event.target.value})
	}

	handleSubmit (event) {
	const templateId = 'template_OixNZRgV';
	this.sendFeedback(templateId, 
		{message_html: this.state.feedback, from_name: this.state.from_name, reply_to: this.state.email, to_name: 'Pheattea', subject: this.state.subject}
	);
	}

	sendFeedback (templateId, variables) {
	emailjs.send(
		'pheattea', templateId , variables, 'user_PAy5oJkbnvIYR3kgE2AsF'
		).then(res => {
			this.setState({
				feedback: '', from_name: '', email: '', isEmailCorrect: this.state.isEmailCorrect
			});
			toast.success('The email was sent successfully');
		})
		.catch(err => toast.error('Something went wrong'));
	}
	
	handleTextInput = (event) => {
		this.setState({
			from_name: event.target.value
		});
	}

	handleTextInputSubject = (event) => {
		this.setState({
			subject: event.target.value
		});
	}

	validateEmail(email) {
		const checker = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return checker.test(email);
	}
	  
	validate = (event) => {
		let email = event.target.value;
		if(email != ''){
			if (this.validateEmail(email)) {
				this.setState({
					isEmailCorrect: true
				})
			} else {
				this.setState({
					isEmailCorrect: false
				})
			}
		return false;
		}
		else {
			this.setState({
				isEmailCorrect: null
			})
		}
	}

  render() {

	const classCheck = this.state.isEmailCorrect ? 'input success-focus' : 
		!this.state.isEmailCorrect && this.state.isEmailCorrect != null ? 'input disabled-focus' 
			: 'input';

	const disabledButton = this.state.isEmailCorrect == null || !this.state.isEmailCorrect ? true : false;

	return (
	<>
	<ToastContainer position="top-center" autoClose={3000} pauseOnHover={false}/>
	<form className="test-mailing">
		<div className="field">
			<p className="control has-icons-left has-icons-right">
				<input className={classCheck} type="email" placeholder="Write your email here!" 
					onChange={this.handleTextInput} onBlur={this.validate}/>
				<span className="icon is-small is-left">
					<FontAwesomeIcon icon={faAt}/>
				</span>
				{ this.state.isEmailCorrect && (
					<span className="icon is-small is-right">
						<FontAwesomeIcon icon={faCheck} color='green'/>
					</span>
				)
				}
				{ this.state.isEmailCorrect != null && !this.state.isEmailCorrect && (
					<span className="icon is-small is-right">
					<FontAwesomeIcon icon={faTimes} color='red'/>
				</span>
				)
				}
				
			</p>
		</div>
		<div className="field">
			<p className="control has-icons-left has-icons-right">
				<input type="text" className="input" placeholder="Write your subject here!" onChange={this.handleTextInputSubject}/>
				<span className="icon is-small is-left">
					<FontAwesomeIcon icon={faShare}/>
				</span>
			</p>
		</div>
		<div>
		<textarea
			id="test-mailing"
			name="test-mailing"
			placeholder="Write your message here!"
			onChange={this.handleChange}
			required
			value={this.state.feedback}
			style={{width: '100%', height: '150px'}}
			className='textarea'
		/>
		</div>
		<input type="button" value="Submit" className="button is-default" onClick={this.handleSubmit} disabled={disabledButton}/>
	</form>
	</>
	)
  }
}
export default SendEmail;