import React from 'react';

class Footer extends React.Component<any, any> {

  constructor(props) {
    super(props);
    this.state = {
      show: true,
      style :{
        opacity: 0,
        transition: 'all 2s ease',
      }
    }
    this.mountStyle = this.mountStyle.bind(this);
  }

  mountStyle() { 
    this.setState({
      style: {
        opacity: 1,
        transition: 'all 1s ease',
      }
    })
  }

  componentDidMount(){
    setTimeout(this.mountStyle, 10)
  }

  render(){
  return (
    <div style={this.state.style}>
      <nav className="navbar is-transparent is-fixed-bottom margin-bottom--9px" role="navigation" aria-label="main navigation">
        <div className="navbar-brand">
          <a className="aStyle navbar-item fontFjalla-Copyright">
            &copy; PHEATTEA
          </a>
        </div>
      </nav>
    </div>
  );
  }
}

export default Footer;