import React from 'react';
import _ from 'lodash';
import Lightbox from 'react-image-lightbox';
import Img from 'react-cool-img';

class ImageColumns extends React.Component<any, any> {
  constructor(props) {
    super(props);
    this.state = {
      show: true,
      style :{
        opacity: 0,
        transition: 'all 2s ease',
      },
      isBoxOpen: false,
      photoIndex: '0'
    }
  }

  openImageBox = (index: any) => () =>{
    this.setState({
      isBoxOpen: true,
      photoIndex: index
    });
  }

  render(){
        const {isBoxOpen,photoIndex} = this.state;
        const {images} = this.props;
        const arrayColumns : JSX.Element[] = [];
        let open = this.openImageBox;
        _.forEach(images, function(image){
        const index = images.indexOf(image);
          arrayColumns.push(
            <div className="column is-2" onClick={open(index)} key={index}>
              <figure className="image is-square"><Img src={image} className='opacity object-fit-image'/></figure>
            </div>
          ); 
        });
  return (
    <>
        { isBoxOpen && (
            <Lightbox
                mainSrc={images[photoIndex]}
                nextSrc={images[(photoIndex + 1) % images.length]}
                prevSrc={images[(photoIndex + images.length - 1) % images.length]}
                onCloseRequest={() => this.setState({ isBoxOpen: false })}
                onMovePrevRequest={() =>
                this.setState({
                    photoIndex: (photoIndex + images.length - 1) % images.length,
                })
                }
                onMoveNextRequest={() =>
                this.setState({
                    photoIndex: (photoIndex + 1) % images.length,
                })
                }
                enableZoom={false}
            />
        )}
    <div style={{margin: '50px'}}>
        <div className="columns is-desktop is-multiline">
            {arrayColumns}
        </div>
    </div>
    </>
  );
  }
}

export default ImageColumns;