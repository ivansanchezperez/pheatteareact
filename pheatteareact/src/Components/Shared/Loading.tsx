import React from 'react';
import { Redirect } from 'react-router-dom';

class Loading extends React.Component<any, any> {
  constructor(props) {
    super(props);
    this.state = {redirectHome: false,
      style :{
      transition: 'all 1s ease',
      // color: '#bb747cad'
    }
  };

    this.redirectToHome = this.redirectToHome.bind(this);
    // this.mountStyle = this.mountStyle.bind(this);
  }

  componentDidMount(){
    // setTimeout(this.mountStyle, 1000);
    setTimeout(this.redirectToHome, 2000);
  }

  // mountStyle = () => { 
  //   this.setState({
  //     style: {
  //       transition: 'all 1s ease',
  //       color: 'black'
  //     }
  //   })
  // }

  redirectToHome(){
    this.setState({
      redirectHome: true
    })
  }

  render(){

    if(this.state.redirectHome){
      return (
        <Redirect to={'/Home'}/>
      )
    }

  return (
    <>
      <div className="level centered-div">
        <p className="level-item has-text-centered">
        </p>
        <p className="level-item has-text-centered">
        </p>
        <p className="level-item has-text-centered fontFjalla-Loading" style={this.state.style}>
          PHEATTEA
        </p>
        <p className="level-item has-text-centered">
        </p>
        <p className="level-item has-text-centered">
        </p>
      </div>
    </>
  );
  }
}

export default Loading;