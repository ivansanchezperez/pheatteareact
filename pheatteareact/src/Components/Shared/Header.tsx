import React from 'react';
import ToogleMenu from './../../images/menuToogle.png';
import {Navbar, Nav, NavLink } from "react-bootstrap";
import { Fragment } from 'react';
class Header extends React.Component<any, any> {

  constructor(props) {
    super(props);
    this.state = {
      show: true,
      style :{
        opacity: 0,
        transition: 'all 2s ease',
      },
      expanded: false,
    }
    this.mountStyle = this.mountStyle.bind(this);
  }

  mountStyle() { 
    
    this.setState({
      style: {
        opacity: 1,
        transition: 'all 1s ease',
      }
    })
  }

  setExpanded = (dataChild: boolean) =>{
    this.setState({
      expanded: dataChild
    });
  }

  componentDidMount(){
    setTimeout(this.mountStyle, 10)
  }

  render() {
    const {expanded} = this.state;

    return (
      <Navbar expanded={expanded} onToggle={() => {
        this.setExpanded(!expanded);
    }} onSelect={() => {
        this.setExpanded(false);
    }} expand="lg">
        <Navbar.Brand>
          <a className="aStyle navbar-item fontFjalla" href="/#/Loading">
            PHEATTEA
          </a>
        </Navbar.Brand>
        {<Fragment>
            <Navbar.Toggle aria-controls="basic-navbar-nav">
                <img className='menu-toogle-button' src={ToogleMenu} alt="Toggle menu icon"/>
            </Navbar.Toggle>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ml-auto nav-items-container align-items-lg-center">
                    <NavLink className="nav-item margin-right-20px margin-left-20px" as={NavLink} href="/#/Menu" onClick={() => {}}>Menu</NavLink>
                    <NavLink className="nav-item margin-right-20px margin-left-20px" as={NavLink} href="/#/Contact" onClick={() => {}}>Contact</NavLink>
                </Nav>
            </Navbar.Collapse>
        </Fragment>}
    </Navbar>
    );
  }
}

export default Header;