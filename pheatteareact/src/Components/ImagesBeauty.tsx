import img1 from '../images/beauty/beauty1.jpg';
import img2 from '../images/beauty/beauty2.jpg';
import img3 from '../images/beauty/beauty3.jpg';
import img4 from '../images/beauty/beauty4.jpg';
import img5 from '../images/beauty/beauty5.jpg';
import img6 from '../images/beauty/beauty6.jpg';

const images = [img1, img2, img3, img4, img5, img6];

export default images;