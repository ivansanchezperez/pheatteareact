import React from 'react';
import SendEmail from './Shared/SendEmail';
import { faInstagram, faLinkedin } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class Contact extends React.Component<any, any> {

  constructor(props) {
    super(props);
    this.state = {
      show: true,
      style :{
        opacity: 0,
        transition: 'all 2s ease',
      },
      styleAll :{
        opacity: 0,
        transition: 'all 2s ease',
      },
    }
    this.mountStyle = this.mountStyle.bind(this);
  }

  mountStyle() { 
    this.setState({
      style: {
        opacity: 1,
        transition: 'all 1s ease',
        marginLeft: '10%',
        marginRight: '10%',
        marginTop: '3%'
      },
      styleAll : {
        opacity: 1,
        transition: 'all 1s ease',
        marginTop: '5%'
      }
    })
  }

  componentDidMount(){
    setTimeout(this.mountStyle, 10)
  }

  render(){
  return (
    <div style={this.state.styleAll}>
      <div className="fontFjalla container fontSize-30px margin-left-10pc margin-top-3pc">
        CONTACT
      </div>
      <div style={this.state.style}>
        <SendEmail/>
      </div>
      <div className="fontFjalla container margin-left-10pc margin-top-3pc">
        <a className="color-black aStyle fontSize-15px" href="https://www.instagram.com/pheattea" target="_blank">
          <FontAwesomeIcon icon={faInstagram} color='black' style={{marginRight: '0.5%'}}/>pheattea
        </a>
        <a className="color-black aStyle margin-left-2pc fontSize-15px" href="https://www.linkedin.com/in/paula-p%C3%A9rez-arias-b89244162/" target="_blank">
          <FontAwesomeIcon icon={faLinkedin} color='black' style={{marginRight: '0.5%'}}/>pheattea
        </a>
      </div>
    </div>
  );
  }
}

export default Contact;