import _ from 'lodash';
import React from 'react';
import ImageGallery from 'react-image-gallery';
import Lightbox from 'react-image-lightbox';
import images from './ImagesHome';
import '../css/image-gallery.scss';
import '../css/carousel.css';

class Home extends React.Component<any,any> {
  _imageIndex: any
  constructor(props: any) {
    super(props);
    this.state = {
      images: [],
      style :{
        opacity: 0,
        transition: 'all 2s ease',
      },
      isBoxOpen: false,
      photoIndex: '0'
    };
    this._imageIndex = React.createRef();
    this.mountStyle = this.mountStyle.bind(this)
  };

  mountStyle() {
    let imagesCarousel: { original: string, thumbnail: string }[] = [];
    _.forEach(images, function(img) {  
      if (img != null) {
        imagesCarousel.push({ "original": img.src, "thumbnail": img.src });
      }
    }) 
    this.setState({
      style: {
        fontSize: 60,
        opacity: 1,
        transition: 'all 1s ease',
      },
      images: imagesCarousel 
    })
  }

  componentDidMount(){
    setTimeout(this.mountStyle, 10);
  }

  openImageBox = () => {
    const index =  this._imageIndex.getCurrentIndex().toString();
    this.setState({
      isBoxOpen: true,
      photoIndex: index
    });
  }

  render() {
    const {style, images, photoIndex, isBoxOpen} = this.state;
    return (
      <>
      { isBoxOpen && (
        <Lightbox
            mainSrc={images[photoIndex].original}
            nextSrc={images[(photoIndex + 1) % images.length].original}
            prevSrc={images[(photoIndex + images.length - 1) % images.length].original}
            onCloseRequest={() => this.setState({ isBoxOpen: false })}
            onMovePrevRequest={() =>
              this.setState({
                photoIndex: (photoIndex + images.length - 1) % images.length,
              })
            }
            onMoveNextRequest={() =>
              this.setState({
                photoIndex: (photoIndex + 1) % images.length,
              })
            }
            enableZoom={false}
        />
      )}
      <div style={style}>
        <ImageGallery items={images} showThumbnails={false} showPlayButton={false} autoPlay={true} showFullscreenButton={false} onClick={this.openImageBox} ref={i => this._imageIndex = i}/>
      </div>
      </>
    )
  }
}

export default Home;