import img1 from '../images/beauty/beauty5.jpg';
import img2 from '../images/editorial/editorial1.jpg';
import img3 from '../images/beauty/beauty6.jpg';
import img4 from '../images/editorial/editorial9.jpg';
import img5 from '../images/lookbook/lookbook3.jpg';

const images = [
    { id: 1, src: img1, title: '1', description: '1' },
    { id: 2, src: img2, title: '2', description: '2' },
    { id: 3, src: img3, title: '3', description: '3' },
    { id: 4, src: img4, title: '4', description: '4' },
    { id: 5, src: img5, title: '5', description: '5' },

];

export default images;