import React from 'react';
import beauty from '../images/beauty/beauty1.jpg'
import editorial from '../images/editorial/editorial3.jpg'
import lookbook from '../images/lookbook/lookbook2.jpg'
import { Redirect } from 'react-router-dom';

class Menu extends React.Component<any,any> {
  constructor(props: any) {
    super(props);
    this.state = {
      style :{
        opacity: 0,
        transition: 'all 2s ease',
        margin: '50px'
      },
      toBeauty: false,
      toEditorial: false,
      toLookBook: false
    };
    this.mountStyle = this.mountStyle.bind(this)
  };

  mountStyle() {
    this.setState({
      style: {
        fontSize: 60,
        opacity: 1,
        transition: 'all 1s ease',
        margin: '50px'
      },
    })
  }

  redirectComponent = (where: string) => () => {

    switch(where){
      case 'Beauty' : this.setState({
        toBeauty: true
      });
      break;
      case 'Editorial' : this.setState({
        toEditorial: true
      });
      break;
      case 'LookBook' : this.setState({
        toLookBook: true
      });
      break;
    }
  };

  componentDidMount(){
    setTimeout(this.mountStyle, 10);
  }

  render() {

    if(this.state.toBeauty)
      return(
        <Redirect to={'/Beauty'}/>
      )
    
    if(this.state.toEditorial)
    return(
      <Redirect to={'/Editorial'}/>
    )

    if(this.state.toLookBook)
    return(
      <Redirect to={'/LookBook'}/>
    )

    return (
      <div style={this.state.style}>
        <div className="tile is-ancestor">
            <div className="tile is-parent container-image" onClick={this.redirectComponent('Beauty')}>
                <article className="tile is-child box">
                <figure className="image">
                  <img src={beauty} className='opacity' alt='beauty'/>
                </figure>
                <div className="card-title">
                  <div className="text">BEAUTY</div>
                </div>
                </article>
            </div>
            <div className="tile is-parent container-image" onClick={this.redirectComponent('Editorial')}>
                <article className="tile is-child box">
                <figure className="image">
                  <img src={editorial} className='opacity' alt='editorial'/>
                </figure>
                <div className="card-title">
                  <div className="text">EDITORIAL</div>
                </div>
                </article>
            </div>
            <div className="tile is-parent container-image" onClick={this.redirectComponent('LookBook')}>
                <article className="tile is-child box">
                <figure className="image">
                  <img src={lookbook} className='opacity' alt='lookbook'/>
                </figure>
                <div className="card-title">
                  <div className="text">LOOKBOOK</div>
                </div>
                </article>
            </div>
            <div className="tile is-parent container-image">
                <article className="tile is-child box">
                <figure className="image">
                  <img src={editorial} className='opacity' alt='male'/>
                </figure>
                <div className="card-title">
                  <div className="text">MALE</div>
                </div>
                </article>
            </div>
        </div>
      </div>
    )
  }
}

export default Menu;