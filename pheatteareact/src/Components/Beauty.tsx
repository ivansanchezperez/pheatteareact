import React from 'react';
import { ImageColumns } from './indexComponents';
import images from './ImagesBeauty';

class Beauty extends React.Component<any, any> {

  constructor(props) {
    super(props);
    this.state = {
      show: true,
      style :{
        opacity: 0,
        transition: 'all 2s ease',
      },
    }
    this.mountStyle = this.mountStyle.bind(this);
  }

  mountStyle() { 
    this.setState({
      style: {
        opacity: 1,
        transition: 'all 1s ease',
      }
    })
  }

  componentDidMount(){
    setTimeout(this.mountStyle, 10)
  }

  render(){
  return (
    <>
        <div style={this.state.style}>
            <ImageColumns images={images}/>
        </div>
    </>
  );
  }
}

export default Beauty;