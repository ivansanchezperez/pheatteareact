import img1 from '../images/lookbook/lookbook1.jpg';
import img2 from '../images/lookbook/lookbook2.jpg';
import img3 from '../images/lookbook/lookbook3.jpg';
import img4 from '../images/lookbook/lookbook4.jpg';
import img5 from '../images/lookbook/lookbook5.jpg';

const images = [img1, img2, img3, img4, img5];

export default images;