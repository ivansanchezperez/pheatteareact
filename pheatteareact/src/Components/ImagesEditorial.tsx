import img1 from '../images/editorial/editorial1.jpg';
import img2 from '../images/editorial/editorial2.jpg';
import img3 from '../images/editorial/editorial3.jpg';
import img4 from '../images/editorial/editorial4.jpg';
import img5 from '../images/editorial/editorial5.jpg';
import img6 from '../images/editorial/editorial6.jpg';
import img7 from '../images/editorial/editorial7.jpg';
import img8 from '../images/editorial/editorial8.jpg';
import img9 from '../images/editorial/editorial9.jpg';
import img10 from '../images/editorial/editorial10.jpg';

const images = [img1, img2, img3, img4, img5, img6, img7, img8, img9, img10];

export default images;