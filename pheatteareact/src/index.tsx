import  React  from 'react';
import * as ReactDOM from "react-dom";
import * as serviceWorker from './serviceWorker';
import { HashRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import { Layout, Loading, Home, Menu, Beauty, Editorial, LookBook, Contact } from "./Components/indexComponents";
import '../src/css/App.css';
import 'bulma/css/bulma.css';
import 'react-image-lightbox/style.css';
import 'bootstrap/dist/css/bootstrap.min.css';

declare let module: any

const PRoute = ({ component: Component, ...rest}) => (
  <Route {...rest} render={props => {
    return (
        <Layout>
          <Component {...props} />
        </Layout>
    )
  }} />
)

ReactDOM.render(
        <Router>  
          <Switch key="switch"> 
            <Route exact sensitive path="/"><Redirect to="/Loading"/></Route>
            <PRoute exact sensitive path="/Home" component={Home}/>
            <Route exact sensitive path="/Loading" component={Loading} />
            <PRoute exact sensitive path="/Menu" component={Menu} />
            <PRoute exact sensitive path="/Beauty" component={Beauty} />
            <PRoute exact sensitive path="/Editorial" component={Editorial} />
            <PRoute exact sensitive path="/LookBook" component={LookBook} />
            <PRoute exact sensitive path="/Contact" component={Contact} />
          </Switch>  
        </Router>  
    ,document.getElementById("root"));

    serviceWorker.register();

if (module.hot) {
   module.hot.accept();
}
